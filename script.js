const d = new Date();
let day = d.getDate();
day = `.day-${day}`

$( document ).ready(function() {
    $(day).removeClass("inactive");
    $(day).addClass("active");
});

$(".close, .overlay").click(function() {
    $(".map-container").fadeOut();
});

$(".box-day").click(function() {
    $(".map-container").fadeIn();
    if ($(this).hasClass("day-1")){
        $(".img-1").fadeIn();  
        $(".dyn-text").text("Tiens, un symbole étrange ?") 
    }

    if ($(this).hasClass("day-2")){
        $(".img-1").fadeIn(function () {
            $(".img-2").fadeIn();
        });
        $(".dyn-text").text("En voilà un deuxième...") 
    }

    if ($(this).hasClass("day-3")){
        $(".img-1").fadeIn(function () {
            $(".img-2").fadeIn(function () {
                $(".img-3").fadeIn();
            });
        });
        $(".dyn-text").text("Jamais 2 sans 3, je suppose ?!") 
    }

    if ($(this).hasClass("day-4")){
        $(".img-1").fadeIn(function () {
            $(".img-2").fadeIn(function () {
                $(".img-3").fadeIn(function () {
                $(".img-4").fadeIn();
                });
            });
        });
        $(".dyn-text").text("4 symboles. Hmm.. à quoi ça pourrait correspondre ?") 
    }

    if ($(this).hasClass("day-5")){
        $(".img-day-5").fadeIn()
        $(".img-2").fadeIn();
        $(".img-3").fadeIn();  
        $(".img-4").fadeIn();
        $(".dyn-text").text("C'est bon, on a déménagé ! ❤️ Tiens, c'est quoi cette croix bleue ?") 
    }

    if ($(this).hasClass("day-6")){
        $(".img-1").fadeIn();
        $(".img-2").fadeIn();
        $(".img-day-6").fadeIn();
        $(".img-4").fadeIn();
        $(".dyn-text").text("Aaaah J1 ensemble ! ❤️ Tiens... J'ai vachement faim ce matin !") 
    }

    if ($(this).hasClass("day-7")){
        $(".img-1").fadeIn();
        $(".img-2").fadeIn();
        $(".img-day-7").fadeIn();
        $(".img-4").fadeIn();
        $(".dyn-text").text('"Sous l\'aile de notre couple, se cache l\'amour le plus blanc"')
    }

    if ($(this).hasClass("day-8")){
        $(".dyn-text").text('Tiens pas de carte cette fois-çi, mais un message : "Au réveil, je suis tout près de toi".')
    }
});
